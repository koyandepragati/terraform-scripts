resource "aws_s3_bucket" "jarcontent" {
  bucket = "jarcontent1234"
}

resource "aws_s3_bucket_object" "jar_hash" {
  bucket = "jarcontent1234"
  key    = "sampleexample"
  source = "${path.module}/lambda_function.py.zip"
  etag   = "${filemd5("${path.module}/lambda_function.py.zip")}"
}
