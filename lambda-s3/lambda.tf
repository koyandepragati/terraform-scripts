
resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<-EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
 }
 EOF
}


resource "aws_lambda_function" "LambdaFunction" {
  depends_on        = ["aws_s3_bucket_object.jar_hash"]
  s3_bucket         = "jarcontent1234"
  s3_key            = "sampleexample"
  function_name     = "welcome"
  description       = "Lambda function for test"
  role              = "${aws_iam_role.iam_for_lambda.arn}"
  #role              = "arn:aws:iam::685422237526:role/arv"
  runtime           = "python3.7"
 # source_code_hash = "${data.aws_s3_bucket_object.jar_hash.body}"
  handler           = "welcome.hello"
 # memory_size       = "1536"
  timeout           = "15"
}
