resource "aws_s3_bucket" "jarcontent" {
  bucket = "jar12345"
}

resource "aws_s3_bucket_object" "jar_hash" {
  bucket = "jar12345"
  key    = "sampleexample1234"
  source = "${path.module}/lambda_function.py.zip"
  etag   = "${filemd5("${path.module}/lambda_function.py.zip")}"
}
